#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass ../UTN-MasterTesis-LyX
\begin_preamble
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                                                                            %%
%%                            ''UTN Tesis LyX''                               %%
%%                                                                            %%
%% Plantilla tesis master Instituto de Posgrado Universidad Técnica del Norte %%
%% Ported to LyX 2.2                                                          %%
%% Version: v2.2                                                              %%
%% Authors: Krishna Kumar                                                     %%
%% Modificado por: Roberto López Hinojosa  & Daniel López Hinojosa            %%
%% Date: 2017/02/14                                                           %%
%% License: MIT License (c) 2013 Krishna Kumar                                %%
%% Based on: https://github.com/kks32/phd-thesis-template/                    %%
%% Git Repo: https://gitlab.com/roberto.lopez/PlantillaTesisMis               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end_preamble
\options a4paper,titlepage,12pt, authoryear
\use_default_options false
\begin_modules
natbibapa
\end_modules
\maintain_unincluded_children false
\language spanish-mexico
\language_package default
\inputencoding default
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command bibtex
\index_command default
\paperfontsize 12
\spacing onehalf
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 0
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style apacite
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle fancy
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
ANÁLISIS E INTERPRETACIÓN DE RESULTADOS
\end_layout

\begin_layout Section
Datos Informativos
\end_layout

\begin_layout Standard
Datos Institución...
\end_layout

\begin_layout Subsection
Título
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
makeatletter
\end_layout

\begin_layout Plain Layout


\backslash
MakeUppercase{``
\backslash
@title''}
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Unidad Ejecutora
\end_layout

\begin_layout Subsection
Beneficiarios
\end_layout

\begin_layout Standard

\series bold
Directos
\series default
, ...
\end_layout

\begin_layout Standard

\series bold
Indirectos
\series default
, ...
\end_layout

\begin_layout Subsection
Ubicación
\end_layout

\begin_layout Standard
La investigación se desarrollará en ...
\end_layout

\begin_layout Subsection
Equipo técnico responsable 
\end_layout

\begin_layout Standard
El equipo técnico responsable esta conformado por ...
\end_layout

\begin_layout Section
Antecedentes de la propuesta
\end_layout

\begin_layout Standard
El presente trabajo de investigación,...
\end_layout

\begin_layout Section
Análisis de factibilidad 
\end_layout

\begin_layout Subsection
Factibilidad técnica
\end_layout

\begin_layout Standard
Para la realización del presente estudio...
\end_layout

\begin_layout Itemize
1
\end_layout

\begin_layout Itemize
2
\end_layout

\begin_layout Itemize
3
\end_layout

\begin_layout Itemize
4
\end_layout

\begin_layout Subsection
Factibilidad organizacional
\end_layout

\begin_layout Standard
El Reglamento ...:
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename Figs/f1.png
	lyxscale 60
	scale 30
	rotateOrigin center

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Organigrama ...
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Factibilidad económica
\end_layout

\begin_layout Standard
El financiamiento del estudio se ...
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset space \hfill{}
\end_inset


\begin_inset Graphics
	filename Figs/f2.png
	lyxscale 80
	scale 60
	rotateOrigin center

\end_inset


\begin_inset space \hfill{}
\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Presupuesto
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Factibilidad operativa
\end_layout

\begin_layout Standard
El estudio se lo realizará ...
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
begin{landscape}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Estructura de descomposición del trabajo
\end_layout

\begin_layout Standard
Se considera la descomposición del diseño de arquitectura de software como
 se describe a continuación:
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement bh
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename Figs/f3.png
	lyxscale 40
	scale 35
	rotateOrigin center

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Estructura de descomposición de trabajo
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
end{landscape}
\end_layout

\end_inset


\end_layout

\begin_layout Section
Metodología
\end_layout

\begin_layout Standard
Considerando ...
\end_layout

\begin_layout Subsection
Proceso de ...
\end_layout

\begin_layout Standard
...pasos generales:
\end_layout

\begin_layout Itemize

\series bold
El ...,
\series default
 xxx
\end_layout

\begin_layout Itemize

\series bold
La ...,
\series default
 xxx
\end_layout

\begin_layout Itemize
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename Figs/f4.png
	lyxscale 50
	scale 40
	rotateOrigin center

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
Modelo de..
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
asdsadsa
\end_layout

\begin_layout Subsection
ETC
\end_layout

\end_body
\end_document
