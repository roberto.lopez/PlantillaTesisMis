#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#%%                                                                            %%
#%%                            ''UTN Tesis LyX''                               %%
#%%                                                                            %%
#%% Plantilla tesis master Instituto de Posgrado Universidad Técnica del Norte %%
#%% Ported to LyX 2.2                                                          %%
#%% Version: v2.2                                                              %%
#%% Authors: Krishna Kumar                                                     %%
#%% Modificado por: Roberto López Hinojosa  & Daniel López Hinojosa            %%
#%% Date: 2017/02/14                                                           %%
#%% License: MIT License (c) 2013 Krishna Kumar                                %%
#%% Based on: https://github.com/kks32/phd-thesis-template/                    %%
#%% Git Repo: https://gitlab.com/roberto.lopez/PlantillaTesisMis               %%
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#% Do not delete the line below; configure depends on this
#  \DeclareLaTeXClass{UTN-MasterTesis-LyX}

# Read the definitions from article.layout
Format 60
Input book.layout


Style Bibliography
	TopSep                	4
	LatexName		References
	LabelString           	"Referencias"
	LabelFont
	  Series	        Bold
	  Size          	normal
	EndFont
End

Style acceptance
        Margin		Static
	LatexType	Environment
	LatexName	acceptance
	Category        FrontMatter
	LabelType       static
	LabelString     "Aceptación del Director"
	LabelBottomSep  0.5

End

Style abstract
	Margin			Static
	LatexType		Environment
	LatexName		abstract
	Category        FrontMatter
	NextNoIndent    1
	LeftMargin      MMM
	RightMargin     MMM
	ParIndent       MM
	ItemSep         0
	TopSep          0.7
	BottomSep       0.7
	ParSep          0
	Align           Block
	LabelType       static
	LabelString     "Resumen"
	LabelBottomSep  0.5
	Font
	  Size          Small
	EndFont
	LabelFont
	  Series        Bold
	  Size          normal
	EndFont

End

Style acknowledgements
	Margin			Static
	LatexType		Environment
	LatexName		acknowledgements
	Category		FrontMatter
	NextNoIndent		1
	LeftMargin		MMM
	RightMargin		MMM
	ParIndent		MM
	ItemSep			0
	TopSep			0.7
	BottomSep		0.7
	ParSep			0
	Align			Block
	LabelType		static
	LabelString           	"Agradecimientos"
	LabelBottomSep        	0.5
	Font
	  Size                	Small
	EndFont
	LabelFont
	  Series              	Bold
	  Size                	Large
	EndFont
End

Style declaration
	Margin                	Static
	LatexType             	Environment
	LatexName             	declaration
	Category              	FrontMatter
	NextNoIndent          	1
	LeftMargin            	MMM
	RightMargin          	MMM
	ParIndent             	MM
	ItemSep               	0
	TopSep               	0.7
	BottomSep            	0.7
	ParSep                	0
	Align                 	Block
	LabelType             	static
	LabelString           	"Declaración"
	LabelBottomSep        	0.5
	Font
	  Size                	Small
	EndFont
	LabelFont
	  Series	        Bold
	  Size                	Large
	EndFont
End



Style dedication
	Margin                	Static
	LatexType             	Environment
	LatexName             	dedication
	Category              	FrontMatter
	NextNoIndent          	1
	LeftMargin            	MMM
	RightMargin           	MMM
	ParIndent             	MM
	ItemSep               	0
	TopSep                	0.7
	BottomSep             	0.7
	ParSep                	0
	Align                 	Block
	LabelType             	static
	LabelString           	"Dedicatoria"
	LabelBottomSep        	0.5
	Font
	  Size                	Small
	EndFont
	LabelFont
	  Series              	Bold
	  Size                	Large
	EndFont
End


