# Universidad Técnica del Note - Master's Thesis Template for LyX 2.2.x

**Author:** Krishna Kumar

**Modified by:** Roberto López Hinojosa & Daniel López Hinojosa

**e-mails:** <e.roberto.lopez@gmail.com>, <danielopezh@gmail.com>

Distributed under MIT License.

## Thesis Information

Go to Document > Settings > LaTeX preamble to modify the title, author, year, department fields.


### Custom Options

To set custom options supported by the class file go to Document > Settings > Document class and enter “a4paper,12pt,numbered,oneside,print,openany” without quotes in the custom field.

**Paper Size:** `a4paper / a5paper ` a4paper (A4 page size default)

**FontSize:** 10pt / 11pt / 12pt `12pt'`(default)

**Layout:** `oneside`, `twoside` or `openany`(twoside and openany default): Printing double side or single side without black page.

**Export Mode:** `print / online` Use `print` for print version with appropriate margins and page layout. Leaving the options field blank will activate `Online` version.

**draft:** For `draft` mode without loading any images (same as draft in book)

**Bibliography style:** To define document class options like bibliography style (numbered / authoryear) go to Document > Settings > Document classs and enter “numbered” or “authoryear” without quotes in the custom field.

**Page Style**

`default (leave empty)`: For Page Numbers in Header (Left Even, Right Odd) and Chapter Name in Header (Right Even) and Section Name (Left Odd). Blank Footer.
`PageStyleI`: Chapter Name next & Page Number on Even Side (Left Even). Section Name & Page Number in Header on Odd Side (Right Odd). Footer is empty.

`PageStyleII`: Chapter Name on Even Side (Left Even) in Header. Section Number % and Section Name in Header on Odd Side (Right Odd). Page numbering in footer

**Font**

`times`: (Recommend using Times). Specifying times option in the document class will use `mathptpx` or `Times` font with Math Support.

`fourier`: fourier font with math support

`default (empty)`: When no font is specified, `Latin Modern` is used as the default font with Math Support.

Go to Document > Settings > Document class and include `times` or `fourier` in the custom field.
